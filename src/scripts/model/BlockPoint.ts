/// <reference path="./Point.ts" />

namespace tetris.model {
    export class BlockPoint extends Point {
        private _c: string;

        constructor(x: number = 0, y: number = 0, c: string = "c0") {
            super(x, y);
            this._c = c;
        }

        get c() {
            return this._c;
        }
    }
}
