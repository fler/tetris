/// <reference path="./Form.ts" />

namespace tetris.model {
    export class Shape {
        private _color: string;
        private _forms: Array<Form>;

        constructor(colorIndex: number, forms: Array<Form>) {
            this._color = `c${colorIndex}`;
            this._forms = forms;
        }

        get color(): string {
            return this._color;
        }

        getForm(index): Form {
            const formsCount = this._forms.length;
            return this._forms[index % formsCount];
        }

        get formsCount(): number {
            return this._forms.length;
        }
    }

    export const SHAPES = [
        // 正方形
        [
            [[0, 0], [0, 1], [1, 0], [1, 1]]
        ],
        // |
        [
            [[0, 0], [0, 1], [0, 2], [0, 3]],
            [[0, 0], [1, 0], [2, 0], [3, 0]]
        ],
        // L
        [
            [[0, 0], [0, 1], [0, 2], [1, 2]],
            [[0, 0], [1, 0], [2, 0], [0, 1]],
            [[0, 0], [1, 0], [1, 1], [1, 2]],
            [[2, 0], [0, 1], [1, 1], [2, 1]]
        ],
        // 反 L
        [
            [[1, 0], [1, 1], [0, 2], [1, 2]],
            [[0, 0], [0, 1], [1, 1], [2, 1]],
            [[0, 0], [1, 0], [0, 1], [0, 2]],
            [[0, 0], [1, 0], [2, 0], [2, 1]]
        ],
        // Z
        [
            [[0, 0], [1, 0], [1, 1], [2, 1]],
            [[1, 0], [0, 1], [1, 1], [0, 2]]
        ],
        // 反 Z
        [
            [[1, 0], [2, 0], [0, 1], [1, 1]],
            [[0, 0], [0, 1], [1, 1], [1, 2]]
        ],
        // T
        [
            [[0, 0], [1, 0], [2, 0], [1, 1]],
            [[1, 0], [0, 1], [1, 1], [1, 2]],
            [[1, 0], [0, 1], [1, 1], [2, 1]],
            [[0, 0], [0, 1], [1, 1], [0, 2]]
        ]
    ].map((formData, i) => {
        const forms = formData.map(points => new Form(points));
        return new Shape(i, forms);
    });
}