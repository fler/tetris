namespace tetris.model {
    export class Form {
        private _points: Array<Point>;
        private _width: number;
        private _height: number;

        constructor(points: Array<Array<number>>) {
            let right: number = 0;
            let bottom: number = 0;

            this._points = points.map(p => {
                const point = new Point(p[0], p[1]);
                right = Math.max(right, point.x);
                bottom = Math.max(bottom, point.y);
                return point;
            });

            this._width = right + 1;
            this._height = bottom + 1;
        }

        get points(): Array<Point> {
            return Array<Point>(0).concat(this._points);
        }

        get width(): number {
            return this._width;
        }

        get height(): number {
            return this._height;
        }
    }
}