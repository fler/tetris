/// <reference path="./Eventable.ts" />

namespace tetris {
    const LEVELS = [0, 100, 300, 600, 1000];

    export class InfoPanel extends Eventable {
        private _score: JQuery;
        private _speed: JQuery;

        constructor(score: JQuery | string, speed: JQuery | string) {
            super();
            this._score = $(score);
            this._speed = $(speed);
        }

        get score(): number {
            return ~~this._score.text() || 0;
        }

        set score(value: number) {
            this._score.text(value || 0);
        }

        get speed(): number {
            return ~~this._speed.val() || 1;
        }

        set speed(value: number) {
            this._speed.val(value || 1);
        }

        get interval(): number {
            return 1100 - this.speed * 100;
        }

        sppedUp() {
            const speed = this.speed + 1;
            if (speed > 9) {
                return;
            }
            this.speed = speed;
            this.trigger("speedUp", {
                speed: speed,
                interval: this.interval
            });
        }

        addByLevel(level: number) {
            this.addScore(LEVELS[level]);
        }

        addScore(score: number) {
            const LEVEL_STEP = 10000;
            const oldScoreLevel = ~~(this.score / LEVEL_STEP);
            this.score = score + this.score;
            const newScoreLevel = ~~(this.score / LEVEL_STEP);
            if (newScoreLevel > oldScoreLevel) {
                this.sppedUp();
            }
        }
    }
}
