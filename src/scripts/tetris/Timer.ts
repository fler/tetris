/// <reference path="./Eventable.ts" />

namespace tetris {
    export class Timer extends Eventable {
        private _interval: number;
        private _timer: number;

        constructor(interval: number) {
            super();
            this._interval = interval;
        }

        get interval(): number {
            return this._interval;
        }

        set interval(value: number) {
            if (value === this._interval) {
                return;
            }

            this._interval = value;
            this.restart();
        }

        restart() {
            this.stop();
            this._timer = setInterval(this.process.bind(this), this._interval);
        }

        stop() {
            clearInterval(this._timer);
        }

        private process() {
            this.trigger("performed");
        }
    }
}
