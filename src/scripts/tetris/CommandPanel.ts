/// <reference path="./Eventable.ts" />

namespace tetris {
    export class CommandPanel extends Eventable {
        private _restart: JQuery;
        private _pause: JQuery;
        private _built: boolean;

        constructor(restart: JQuery | string, pause: JQuery | string) {
            super();
            this._restart = $(restart);
            this._pause = $(pause);
            this.build();
        }

        build(): CommandPanel {
            if (!this._built) {
                this.internalBuild();
            }
            return this;
        }

        internalBuild() {
            this._built = true;

            this._restart.on("click", () => {
                this.trigger("restart");
            });

            this._pause.on("click", () => {
                this.trigger("pause");
            });
        }
    }
}
