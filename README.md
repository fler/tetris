# jsTetris - 网页 JavaScript 版俄罗斯方块

> SegmentFault [边城客栈 专栏](https://segmentfault.com/blog/jamesfancy)  
> jamesfancy (jamesfancy@126.com)

---


## 采用技术

- HTML5
- LESS
- JavaScript (ES2015) 

## 源码结构

| 目录/文件 | 说明 |
| ---- | ---- |
| `index.html` | 页面（唯一的页面） |
| `lib` | 第三方脚本库，通过 bower 管理 |
| `js` | 页面中引入的脚本，从 `app/src` 生成 |
| `css` | 页面中引入的样式表，从 `app/ess` 生成 |
| `src/` | 前端脚本和样式表源码 |
| `+ src/less` | 样式表源码 |
| `+ src/scripts` | 脚本源码 |

## 环境搭建

虽然是前端，仍然采用了项目构建的思想，目标脚本和样式表通过构建生成。构建在 [NodeJs 6<sup>+</sup>][nodejs] 环境下进行。

开发中使用了 [TypeScript][typescript]，需要 TypeScript 语言服务。虽然大多数支持 TypeScript 的编辑器或 IDE 已经内置了 TypeScript 语言服务支持，可能可能不是最新版本。可以通过 npm 安装最新版本的 TypeScript，之后通过 `tsc --version` 检查安装的版本。

```bash
$ npm install -g typescript
$ tsc --version
```

> 命令行看起来像是 Linux，但实际是在 Windows 下使用了 [git-bash][git-win]。

## 构建

所有前端源码都在 `src` 目录下，通过 [gulp][gulp] 工具构建。脚本采用 TypeScript 编写，使用了较新的语言特性，需要 [glup-typescript][gulp-typescript] 及 [gulp-babel][gulp-babel] 工具来生成最终兼容 ES5 的脚本。样式表使用 LESS 编号，需要 [gulp-less][gulp-less] 来生成 `.css` 样式表。

页面中需要引入 [jQuery][jquery] 库。jQuery 库脚本通过 npm 引入，并由 gulp 在构建时拷贝到 `js` 目录下供页面引用。

```bash
$ cd src
$ npm install
$ gulp build
```

## 运行

构建完成之后通过浏览器打开 `index.html` 即可开始游戏。

## 操作

| 快捷键 | 功能 |
| ----: | ---- |
| `[UP]` | 方块旋转（顺时针） |
| `[LEFT]` | 向左移动 |
| `[RIGHT]` | 向右移动 |
| `[DOWN]` | 向下移动 |
| `[SPACE]` | 直降到底 |

---

## 相关阅读：SegmentFault 上的系列博文

- [JavaScript 版俄罗斯方块](https://segmentfault.com/a/1190000006919702)
- [JavaScript 版俄罗斯方块 - 重构](https://segmentfault.com/a/1190000007063852)
- [JavaScript 版俄罗斯方块 - 转换为 TypeScript](https://segmentfault.com/a/1190000007074816)

[jquery]: http://jquery.com/
[typescript]: http://www.typescriptlang.org/
[git-win]: https://git-scm.com/download/win
[nodejs]: http://nodejs.org/
[gulp]: http://gulpjs.com/
[less]: http://lesscss.org/
[npm]: https://www.npmjs.com/
[gulp-typescript]: https://www.npmjs.com/package/gulp-typescript
[gulp-babel]: :https://www.npmjs.com/package/gulp-babel
[gulp-less]: :https://www.npmjs.com/package/gulp-less
